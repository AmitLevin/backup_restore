# Over-View #
This is a small tool for automatic backup and restore on google drive. 
Using it will allow you to keep certain files and configurations of your linux computer automaticly backed-up and easily restored in the case of damage or new computer. 

## Installing ##

    git clone https://bitbucket.org/AmitLevin/backup_restore ~/.config/backup_restore
    cd ~/.config/backup_restore
    ./install

This will prompt you to enter a website, log into your google account and paste in the terminal a code you will be given. Do it and click enter. 

## Configurating ##
The tool needs to fit your own computer.
It comes with 4 basic files that allow you to change it's behaviour.
The files come packed with an example of use.
### what_to_save ###
Write here full path for every file / directory you want backed-up. Each in it's own line.
### not_to_save ###
Supports wildcards for general expressions that should not be saved.
### what_to_install ###
Write here, each in it's own line, apt repositories to be installed on your new computer. 
### run_on_restore ###
Write here aditional scripts to be executed on restoration. 

## restoration ##
Just install this repository again and run:
    ./restore

* Tested on ubuntu 14.04

# Contact # #
amitlevin6@gmail.com